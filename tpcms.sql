/*
Navicat MySQL Data Transfer

Source Server         : 本地资源数据库
Source Server Version : 50635
Source Host           : localhost:3306
Source Database       : tpcms

Target Server Type    : MYSQL
Target Server Version : 50635
File Encoding         : 65001

Date: 2018-07-20 10:30:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `cwcms_auth_group`
-- ----------------------------
DROP TABLE IF EXISTS `cwcms_auth_group`;
CREATE TABLE `cwcms_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(100) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `rules` char(80) NOT NULL DEFAULT '',
  `desc` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cwcms_auth_group
-- ----------------------------
INSERT INTO `cwcms_auth_group` VALUES ('4', '超级管理员', '1', '1,2,5,6,8,3,4,7,9,10', '最高权限');
INSERT INTO `cwcms_auth_group` VALUES ('13', '测试', '1', '1,2,5,6,8,3,4', '测试');

-- ----------------------------
-- Table structure for `cwcms_auth_group_access`
-- ----------------------------
DROP TABLE IF EXISTS `cwcms_auth_group_access`;
CREATE TABLE `cwcms_auth_group_access` (
  `uid` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cwcms_auth_group_access
-- ----------------------------
INSERT INTO `cwcms_auth_group_access` VALUES ('1', '4');
INSERT INTO `cwcms_auth_group_access` VALUES ('6', '4');
INSERT INTO `cwcms_auth_group_access` VALUES ('7', '11');
INSERT INTO `cwcms_auth_group_access` VALUES ('8', '4');
INSERT INTO `cwcms_auth_group_access` VALUES ('9', '4');
INSERT INTO `cwcms_auth_group_access` VALUES ('10', '13');

-- ----------------------------
-- Table structure for `cwcms_auth_rule`
-- ----------------------------
DROP TABLE IF EXISTS `cwcms_auth_rule`;
CREATE TABLE `cwcms_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(80) NOT NULL DEFAULT '' COMMENT '规则标识',
  `title` char(20) NOT NULL DEFAULT '' COMMENT '权限名称',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '类型',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1为启用0为禁止',
  `condition` char(100) NOT NULL DEFAULT '',
  `pid` int(11) NOT NULL COMMENT '上级菜单id',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cwcms_auth_rule
-- ----------------------------
INSERT INTO `cwcms_auth_rule` VALUES ('1', 'admin/admin/rule', '管理员管理', '1', '1', '', '0');
INSERT INTO `cwcms_auth_rule` VALUES ('2', 'admin/admin/permission', '权限管理', '1', '1', '', '1');
INSERT INTO `cwcms_auth_rule` VALUES ('3', 'admin/admin/admin_list', '管理员列表', '1', '1', '', '1');
INSERT INTO `cwcms_auth_rule` VALUES ('4', 'admin/admin/role', '角色管理', '1', '1', '', '1');
INSERT INTO `cwcms_auth_rule` VALUES ('5', 'admin/admin/permission_add', '添加权限节点', '1', '1', '', '2');
INSERT INTO `cwcms_auth_rule` VALUES ('6', 'admin/admin/permission_edit', '编辑权限节点', '1', '1', '', '2');
INSERT INTO `cwcms_auth_rule` VALUES ('7', 'admin/member/index', '会员管理', '1', '1', '', '0');
INSERT INTO `cwcms_auth_rule` VALUES ('8', 'admin/admin/del', '删除权限节点', '1', '1', '', '2');
INSERT INTO `cwcms_auth_rule` VALUES ('9', 'admin/index/index', '首页', '1', '1', '', '0');
INSERT INTO `cwcms_auth_rule` VALUES ('10', 'admin/index/welcome', '欢迎页', '1', '1', '', '0');

-- ----------------------------
-- Table structure for `cwcms_members`
-- ----------------------------
DROP TABLE IF EXISTS `cwcms_members`;
CREATE TABLE `cwcms_members` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `nickname` varchar(255) NOT NULL COMMENT '用户昵称',
  `username` varchar(255) NOT NULL COMMENT '用户登录名',
  `password` varchar(255) NOT NULL COMMENT '登录密码',
  `phone` varchar(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否禁用 1为启用 0为禁用',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱地址',
  `createtime` varchar(11) NOT NULL,
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cwcms_members
-- ----------------------------
INSERT INTO `cwcms_members` VALUES ('1', 'ChuckWe', 'ChuckWe', '$2y$08$xmU9V3xgZKcrWRD05oSie.AsqXknjAPbMOan9pbBPjhjzH5wvyN42', '13163256175', '1', '312906342@qq.com', '1531991313', null);
INSERT INTO `cwcms_members` VALUES ('2', 'ccc', 'aaaa', '$2y$10$jNW7JXA5CgSLnXN0bTa1y.09qm/h2gAqc6c252US8wTkO2boHFXDq', '13163256175', '1', 'ewqewqewq', '1531991313', null);
INSERT INTO `cwcms_members` VALUES ('3', '', '', '$2y$08$tjXCZESyrmt4gJDclP6CjewTmTE.H3DfbJlA.Cxhb2H3LTNJsnuBq', '', '1', '', '1532052762', '');

-- ----------------------------
-- Table structure for `cwcms_menu`
-- ----------------------------
DROP TABLE IF EXISTS `cwcms_menu`;
CREATE TABLE `cwcms_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menuname` varchar(255) NOT NULL COMMENT '菜单名称',
  `href` char(255) DEFAULT NULL COMMENT '控制器/方法',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  `pid` int(11) NOT NULL COMMENT '父级ID',
  `createtime` int(11) DEFAULT NULL COMMENT '创建时间',
  `icon` char(10) DEFAULT NULL COMMENT '图标',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of cwcms_menu
-- ----------------------------
INSERT INTO `cwcms_menu` VALUES ('1', '管理员管理', null, '0', '0', null, null);
INSERT INTO `cwcms_menu` VALUES ('2', '权限管理', 'admin/admin/permission', '0', '1', null, null);
INSERT INTO `cwcms_menu` VALUES ('3', '管理员列表', 'admin/admin/admin_list', '0', '1', null, null);
INSERT INTO `cwcms_menu` VALUES ('4', '角色管理', 'admin/admin/role', '0', '1', null, null);
INSERT INTO `cwcms_menu` VALUES ('5', '会员信息', null, '0', '0', null, null);
INSERT INTO `cwcms_menu` VALUES ('7', '会员管理', 'admin/member/member_list', '0', '5', null, null);
INSERT INTO `cwcms_menu` VALUES ('8', '节点管理', 'admin/urlsite/urlsite_list', '0', '6', null, null);
INSERT INTO `cwcms_menu` VALUES ('9', '菜单管理', null, '0', '0', null, null);
INSERT INTO `cwcms_menu` VALUES ('10', '菜单列表', 'admin/admin/menu_list', '0', '9', null, null);
INSERT INTO `cwcms_menu` VALUES ('11', '资讯管理', '', '0', '0', null, '');
INSERT INTO `cwcms_menu` VALUES ('13', '文章管理', 'admin/articlemation/article_list', '0', '11', null, '');

-- ----------------------------
-- Table structure for `cwcms_urlsite`
-- ----------------------------
DROP TABLE IF EXISTS `cwcms_urlsite`;
CREATE TABLE `cwcms_urlsite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `urlsitename` varchar(255) NOT NULL COMMENT '节点标题',
  `searchurl` varchar(255) NOT NULL COMMENT '节点搜索链接',
  `searchlist` text NOT NULL COMMENT '节点搜索列表规则',
  `searchmenu` varchar(255) NOT NULL COMMENT '节点目录规则',
  `searchar` varchar(255) NOT NULL COMMENT '节点内容页规则',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `urlindex` varchar(255) DEFAULT NULL COMMENT '节点主页',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cwcms_urlsite
-- ----------------------------
INSERT INTO `cwcms_urlsite` VALUES ('1', '搜狗文学', 'http://www.sgxsw.com/s.php?q=', 'h4>a|text,.bookimg>a>img|src,h4>a|href,.update>a|text,.update>a|href,.author|text,.update>a|text,.bookinfo>p|text', '.info>h2|text,.cover>img|src,.small>span:nth-child(1)|text,.small>span:nth-child(4)|text,.small>span:nth-child(3)|text,.intro|text,dd>a|text,dd>a|href ', 'dadasd', '1', 'http://www.sgxsw.com');
INSERT INTO `cwcms_urlsite` VALUES ('2', '顶点文学', 'http://zhannei.baidu.com/cse/search?s=8053757951023821596&q=', '#results > div.result-list > div:nth-child(1) > div.result-game-item-detail > h3 > a|title,#results > div.result-list > div:nth-child(1) > div.result-game-item-pic > a > img|src,#results > div.result-list > div:nth-child(1) > div.result-game-item-detail > h3 > a|href,#results > div.result-list > div:nth-child(1) > div.result-game-item-detail > div > p:nth-child(4) > a|text,#results > div.result-list > div:nth-child(1) > div.result-game-item-detail > div > p:nth-child(4) > a|href,.result-game-item-info>p>span:eq(1)|text,.result-game-item-info>p:eq(2)>span:eq(1)|text,#results > div.result-list > div:nth-child(1) > div.result-game-item-detail > p|text', '123', '123', '1', 'http://www.23us.so/');

-- ----------------------------
-- Table structure for `cwcms_users`
-- ----------------------------
DROP TABLE IF EXISTS `cwcms_users`;
CREATE TABLE `cwcms_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(60) NOT NULL DEFAULT '' COMMENT '用户名',
  `password` varchar(64) NOT NULL DEFAULT '' COMMENT '登录密码：MD5加密',
  `avatar` varchar(255) DEFAULT '' COMMENT '用户头像',
  `email` varchar(100) DEFAULT '' COMMENT '登录邮箱',
  `phone` bigint(11) DEFAULT NULL COMMENT '手机号',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '用户状态，0：禁止 1：正常 2：未激活',
  `register_time` int(10) DEFAULT '0' COMMENT '注册时间',
  `last_login_ip` varchar(16) DEFAULT '' COMMENT '最后登录IP',
  `last_login_time` int(10) DEFAULT NULL COMMENT '最后登录时间',
  `uid` int(11) NOT NULL,
  `del` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否停用',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cwcms_users
-- ----------------------------
INSERT INTO `cwcms_users` VALUES ('1', 'ChuckWe', 'fa575be02cfe45f09966f14c015cb1c1', '', '312906342@qq.com', '13163256175', '1', '1524562216', '127.0.0.1', '1532047773', '4', '1');
INSERT INTO `cwcms_users` VALUES ('10', 'ceshi', 'fa575be02cfe45f09966f14c015cb1c1', '', '1232131@163.com', '15926533423', '1', '1526350500', '127.0.0.1', '1526432734', '13', '1');
