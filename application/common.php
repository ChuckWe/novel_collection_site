<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------


function success($msg='成功',$data=[],$code=1){
    $data = ['code'=>$code,'msg'=>$msg,'data'=>$data];
    return json($data);
}

function error($msg='失败',$data=[],$code=0){
    $data = ['code'=>$code,'msg'=>$msg,'data'=>$data];
    return json($data,$code);
}
// 应用公共文件
function getTree($list,$pid=0,$level=0){
	static $tree = array();
	foreach ($list as $row) {
		if($row['pid'] == $pid){
			$row['level'] = $level;
			$tree[] = $row;
			getTree($list,$row['id'], $level + 1);
		}
	}

		return $tree;
}

function getSon($data, $pId=0,$level=0)
{
$tree = '';
foreach($data as $k => $v)
{
  if($v['pid'] == $pId)
  {        //父亲找到儿子
   $v['level'] = $level;
   $v['pid'] = getSon($data, $v['id'],$level+1);
   $tree[] = $v;
  }
}
return $tree;
}

function curl_get($url){
	$curl = curl_init(); // 启动一个CURL会话
	$headers = 'Content-Type:application/json;charset=utf-8;';
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HEADER, $headers);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 2); // 跳过证书检查
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);  // 从证书中检查SSL加密算法是否存在
    $tmpInfo = curl_exec($curl);     //返回api的json对象
    //关闭URL请求
    curl_close($curl);
    return $tmpInfo;    //返回json对象
}

function curl_post($url,$data){
	$curl = curl_init(); // 启动一个CURL会话
    $headers = 'Content-Type:application/json;charset=utf-8;';
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 对认证证书来源的检查
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0); // 从证书中检查SSL加密算法是否存在
    curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']); // 模拟用户使用的浏览器
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1); // 使用自动跳转
    curl_setopt($curl, CURLOPT_AUTOREFERER, 1); // 自动设置Referer
    curl_setopt($curl, CURLOPT_POST, 1); // 发送一个常规的Post请求
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data); // Post提交的数据包
    curl_setopt($curl, CURLOPT_TIMEOUT, 30); // 设置超时限制防止死循环
    curl_setopt($curl, CURLOPT_HEADER, 0); // 显示返回的Header区域内容
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 获取的信息以文件流的形式返回
    $tmpInfo = curl_exec($curl); // 执行操作
    if (curl_errno($curl)) {
        echo 'Errno'.curl_error($curl);//捕抓异常
    }
    curl_close($curl); // 关闭CURL会话
    return $tmpInfo;    //返回json对象
}

function urlbasecode($name){
	$name = urlencode($name);
	return strtoupper($name);
}

function ajaxShow($data='',$status=200,$msg='操作成功',$icon='1'){
    $json = $data;
    $json['status'] = $status;
    $json['msg'] = $msg;
    $json['icon'] = $icon;
    return json($json);
}

function rand_nums($min,$max,$time){
    $numbers = [];
    for ($i=0; $i < $time; $i++) { 
        $n = mt_rand($min,$max);
        while (in_array($n,$numbers)) {
            $n = mt_rand($min,$max);
        }
        $numbers[$i] = $n;
    }
    sort($numbers);
    return $numbers;
    

}

function range_rand($min,$max,$time){
    $values = range($min,$max,1);
    shuffle($values);
    $numbers =[];
    for ($i=0; $i < $time ; $i++){
    $n = array_rand($values,1);
    $numbers[$i] = $values[$n];
    unset($values[$n]);
    }
    sort($numbers);
    return $numbers;
    
}