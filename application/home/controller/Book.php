<?php
namespace app\home\controller;
use think\Request;
use think\Db;
use think\QL;
use think\Controller;
use think\Session;
use think\Jump;
/**
 * 详情页控制器
 * @authors ChuckWe (s312906342@gmail.com)
 * @date    2018-05-29 10:24:49
 * @version $Id$
 */

class Book extends Common {
    
    public function xiangqing1 (){
    	$url = trim(input('param.url'));
    	$id = intval(input('param.id'));
    	$list = Db::name('urlsite')->find($id);
    	$rules = explode(',', $list['searchmenu']);
    	$rule = array(
    		'bookname' => explode('|',$rules[0]),
    		'bookimg'  => explode('|',$rules[1]),
    		'author'   => explode('|',$rules[2]),
    		'bookwords' => explode('|',$rules[3]),
    		'state'	   => explode('|',$rules[4]),
    		'intro'	   => explode('|',$rules[5]),
    		'menutitle' => explode('|',$rules[6]),
    		'url'	   => explode('|',$rules[7]),
    	);
    	$query = new \QL\QueryList();
    	$query->setLog('./log/ql.log');
    	$listinfo = $query->Query($url,$rule,'','UTF-8')->getData();
    	$info = array();
    	$info = $listinfo[0];
    	$bookwords = '';
    	if(preg_match('/\d+/',$info['bookwords'],$arr)){
       		$bookwords = ceil($arr[0]/10000);
    	}
    	
    	$author = explode('：',$info['author'] );
    	$author = $author[1];
    	
    	$this->assign('info',$info);
    	$this->assign('bookwords',$bookwords);
    	$this->assign('author',$author);
    	unset($listinfo[0]);
    	$this->assign('listinfo',$listinfo);
    	$this->assign('list',$list);
    	return $this->fetch();
        
    }

    public function xiangqing(){
        $id = trim(input('param.id'));
        session::set('book_id',$id);
        $sid = input('param.sid');

        $url = 'http://api.zhuishushenqi.com/toc?view=summary&book='.$id;
        $listinfo = curl_get($url);
        $listinfo=json_decode($listinfo,true);
        if(!empty($sid)){
           $link = 'http://api.zhuishushenqi.com/toc/'.$sid.'?view=chapters';
           $this->assign('sid',$sid);
        }else{
            $link = 'http://api.zhuishushenqi.com/toc/'.$listinfo[0]['_id'].'?view=chapters';
            $this->assign('sid',$listinfo[0]['_id']);
        }
        
        //$link = $listinfo[0]['link'].'?view=chapters';
        $list = curl_get($link);
        $list = json_decode($list,true);
        $booklink = 'http://api.zhuishushenqi.com/book/'.$id;
        $bookinfo = curl_get($booklink);
        $bookinfo =json_decode($bookinfo,true);
        $bookinfo['wordCount'] = round(floatval($bookinfo['wordCount']/10000),2);
        $bookinfo['updated'] = strtotime($bookinfo['updated']);
        $updated = time()-$bookinfo['updated'];
        if($updated<60){
            $updated = '刚刚更新';
        }elseif($updated<3600){
            $updated = ceil($updated/60).'分钟前更新';
        }elseif($updated<86400){
            $updated = ceil($updated/3600).'小时前更新';
        }elseif($updated<2592000){
            $updated = ceil($updated/86400).'天前更新';
        }elseif($updated<31536000){
            $updated = ceil($updated/2592000).'月前更新';
        }else{
            $updated = ceil($updated/31536000).'年前更新';
        }
        $bookinfo['updated'] = $updated;
        $bookinfo['cover'] = substr(urldecode($bookinfo['cover']),7);
        $this->assign('bookinfo',$bookinfo);
        $this->assign('listinfo',$list);

        return $this->fetch();

    }

    public function kanshu(){
        //home/book/kanshu.html?sid=577b47aebd86a4bd3f8bf1f4&k=1&url=http://www.biquge.la3587183.html
        //www.tpcms.com/home/book/kanshu.html?sid=577b47aebd86a4bd3f8bf1f4&k=2&url=http://www.biquge.la3587182.html
        $sid = input('param.sid');
        $key = input('param.k');
        $listurl = 'http://api.zhuishushenqi.com/toc/'.$sid.'?view=chapters';
        $curlist = curl_get($listurl);
        $curlist = json_decode($curlist,true);
       
            $count = count($curlist['chapters'])+1;
            $data['upkey'] = $key -1;
            $data['nextkey'] = $key+1;
            
            if($key-1 <= 0){
            $data['upchapter'] = '';
            }else{
                $data['upchapter'] = 'sid='.$sid.'&k='.$data['upkey'].'&url='.$curlist['chapters'][$key-2]['link'];
            }
            
            if($key+1 >= $count){
                $data['nextchapter'] = '';
            }else{
                $data['nextchapter'] = 'sid='.$sid.'&k='.$data['nextkey'].'&url='.$curlist['chapters'][$key]['link'];
            }
        
        $id = session::get('book_id');
        $data['menu'] = 'id='.$id.'&sid='.$sid;
        
        $this->assign('data',$data);
        $url = Request::instance()->url();
        $url = substr($url,strripos($url,':')-4);


        $url = urlencode($url);
        $curlurl = 'http://chapterup.zhuishushenqi.com/chapter/'.$url;
        $article_body = curl_get($curlurl);
        $article_body = json_decode($article_body,true);
       
        
        if($article_body['ok'] === false){
            $this->redirect('common/wrong');
        }
        $chapter = $article_body['chapter'];
        if(empty($chapter['cpContent'])){
            $chapter['cpContent'] = $chapter['body'];
        }
        $content = str_replace("\n","<br/>",$chapter['cpContent']);
        $this->assign('content',$content);
        $this->assign('chapter',$chapter);
        return $this->fetch();
    }

    public function changeurl(){
        $data = input('param.');
        $url = 'http://api.zhuishushenqi.com/toc?view=summary&book='.$data['id'];
        $listinfo = curl_get($url);
        $listinfo=json_decode($listinfo,true);
        foreach ($listinfo as $k => $v) {
                
                $v['updated'] = strtotime($v['updated']);
                $updated = time()-$v['updated'];
                if($updated<60){
                    $updated = '刚刚更新';
                }elseif($updated<3600){
                    $updated = ceil($updated/60).'分钟前更新';
                }elseif($updated<86400){
                    $updated = ceil($updated/3600).'小时前更新';
                }elseif($updated<2592000){
                    $updated = ceil($updated/86400).'天前更新';
                }elseif($updated<31536000){
                    $updated = ceil($updated/2592000).'月前更新';
                }else{
                    $updated = ceil($updated/31536000).'年前更新';
                }
                $listinfo[$k]['updated'] = $updated;
        }
       
        $this->assign('data',$data);
        $this->assign('listinfo',$listinfo);
        
        return $this->fetch();
    }

    public function python(){
        $url = 'http://caiji.chuckwe.top/api_task/2/ceshi';
        $listinfo = curl_get($url);
        $listinfo = json_decode($listinfo,True);
        echo print_r($listinfo);
    }
}