<?php
namespace app\home\controller;
use think\Controller;
use think\Session;
use think\Db;
use think\Request;
/**
 * 前台登录和注册模块
 * @authors ChuckWe (s312906342@gmail.com)
 * @date    2018-07-19 16:17:46
 * @version $Id$
 */

class Login extends Controller {
    
    public function regist(){
    	
    	if(Request::instance()->isPost()){
    		$data['username'] = trim(input('post.username'));
    		$password1 = trim(input('param.password1'));
    		$password2 = trim(input('param.password2'));
    		$data['nickname'] = trim(input('param.nickname'));
    		$data['phone'] = trim(input('param.tel'));
    		$data['email'] = trim(input('param.email'));
    		$data['createtime'] = time();
    		$usersinfo = Db::name('members')->where('username',$data['username'])->find();
    		if($usersinfo){
    			return $this->error('账号已存在!');
    		}
    		if($password1 !== $password2){
    			return $this->error('重复密码错误!');

    		}else{
    			$opition = array('cost' =>8);
			  	$data['password'] = password_hash($password2,PASSWORD_BCRYPT,$opition);
			  	$in = Db::name('members')->insert($data);
			  	if($in){
			  		return $this->success('注册成功,请登录！');
			  	}else{
			  		return $this->error('注册失败！');
			  	}
    		}

    	}
    	return $this->fetch();
    }

    public function login(){
    	if(Request::instance()->isPost()){
    		$username = trim(input('param.username'));

    		$resultinfo = Db::name('members')->where('username',$username)->find();
    		if(!$resultinfo){
    			return $this->error('没有此账号！');
    		}
    		$password = trim(input('param.password'));
    		if(!password_verify($password,$resultinfo['password'])){
    			return $this->error('请检查用户名或密码！');
    		}else{
    			Session::set('userinfo',$resultinfo);
    			
    			return $this->success('登录成功!','home/search',3);
    		}

    	}
    	return $this->fetch();
    }
}