<?php
namespace app\home\controller;
use think\Request;
use think\Db;
use think\QL;
/**
 * 详情页控制器
 * @authors ChuckWe (s312906342@gmail.com)
 * @date    2018-05-29 10:24:49
 * @version $Id$
 */

class Home extends Common {
    
    public function index (){
    	
        return $this->fetch();
    }

    public function search(){

    	return $this->fetch();
    }

    public function search_list(){
    	if(Request::instance()->isPost()){
    		$bookname =trim(input('param.bookname'));
    		$url = 'http://api.zhuishushenqi.com/book/fuzzy-search?start=0&limit=100&query=';
    		$bookname = urlbasecode($bookname);
    		$url = $url . $bookname;
    		$listinfo = curl_get($url);
    		$listinfo=json_decode($listinfo,true);
    		$list = array();
    		foreach ($listinfo['books'] as $key => $val) {
    			$list[$key] = $val;
    			$list[$key]['cover'] = substr(urldecode($val['cover']),7);
    			
    		}
    		$this->assign('listinfo',$list);
    		

    	}
    	return $this->fetch();
    	
    }

    public function search_ceshi(){
    	$query = new \QL\QueryList();
		$query->setLog('./log/ql.log');
        $bookurl='https://www.kanshula.com/book/mushenji/';
        $range = '#list>dl>dd';
        $rules = array(
            'bookname' => array('a','text'),
            'bookurl' => array('a','href'),
        );
        
        $listinfo = $query->Query($bookurl,$rules,$range,'UTF-8')->data;
        
        die(print_r($listinfo));
        return var_dump($listinfo);
    	if(Request::instance()->isPost()){
    		$bookname = input('param.bookname');

    		$list = Db::name('urlsite')->select();

    		$listinfo = array();
    		$commoninfo = array();
    		foreach ($list as $key => $val) {
    			$bookurl = $val['searchurl'].$bookname;
    			$rules = explode(',', $val['searchlist']);
    			$url = $val['urlindex'];
    			$sitename = $val['urlsitename'];
    			$rule = array(
		    		'bookname' => explode('|',$rules[0]),
		    		'bookimg'  => explode('|',$rules[1]),
		    		'url'   => explode('|',$rules[2]),
		    		'arname' => explode('|', $rules[3]),
		    		'lastar' => explode('|',$rules[4]),
		    		'author'	   => explode('|',$rules[5]),
		    		'uptime'	   => explode('|',$rules[6]),
		    		'desc'		   => explode('|', $rules[7]),
		    	);

		    	$listinfo[$key] = $query->Query($bookurl,$rule,'','UTF-8')->getData(function($item) use($url,$sitename){
		    		if(strstr($item['bookimg'],'http')){
		    			$item['title'] = $sitename;
		    			return $item;
		    		}else{
		    			$item['bookimg'] = $url.$item['bookimg'];
		    			$item['url'] = $url.$item['url'];
       					$item['lastar'] = $url.$item['lastar'];
       					$item['title'] = $sitename;
       					return $item;
		    		}
    			});
    			
    		}
    		die(print_r($listinfo));
    		$this->assign('listinfo',$listinfo);
    	}
    	return $this->fetch();
    		    	
    }
    public function ceshi1(){
        $query = new \QL\QueryList();
        $query->setLog('./log/ql.log');
        $url ='https://www.kuaiyankanshu.net/742428/chapterlist.html';
        $range = 'div.card>div.body>ul.chapterlist>li';
        $rules = array('title'=>array('span.n>a','text'),'href'=>array('span.n>a','href'),'yuan'=>array('span.s>a','text'));
        $listinfo = $query->Query($url,$rules,$range,'UTF-8')->data;
        die(var_dump($listinfo));
    }
    public function ceshi2(){
        $zym_6='斗破苍穹';
        $url = 'http://bookshelf.html5.qq.com/data/ajax?m=search&start=1&hotwords=1&resourcename='.urlencode($zym_6);
        die(var_dump($url));

    }
}