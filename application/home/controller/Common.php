<?php
namespace app\home\controller;
use think\Controller;
use think\Session;
use think\Db;
/**
 * 公共控制器继承Controller
 * @authors ChuckWe (s312906342@gmail.com)
 * @date    2018-05-30 15:14:26
 * @version $Id$
 */

class Common extends Controller {
    
    function _initialize(){
       
    }
    public function wrong(){
    	return $this->fetch();
    }
}