<?php
return[
    'template'  =>  [
        'type'  =>  'Think',
        'view_path' =>  '../template/home/',
        'view_suffix'   =>  'html',
        'view_depr' =>  DS,
        'tpl_bein'  =>  '{',
        'tpl_end'   =>  '}',
        'taglib_begin'  =>  '{',
        'taglib_end'    =>  '}',
    ],
];