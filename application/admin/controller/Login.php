<?php
namespace app\admin\controller;
use think\Controller;
use think\Validate;
use think\Request;
use think\Db;
use think\Session;
use think\Jump;
/**
 * 登录类，包含登出，验证数据库等等
 * @authors ChuckWe (s312906342@gmail.com)
 * @date    2018-04-19 14:41:31
 * @version $Id$
 */

class Login extends Controller {
    
    public function login()
    {
    	//判断是否存在登录session,有则不允许进入登录页跳转到登录欢迎页面
    	if(!empty(Session::get('loginid'))){
    		$this->redirect(url('admin/index/index'));
    	}
    	//不存在session则进入登录页面，优先验证验证码
    	if(Request::instance()->isPost()){
    		$captcha = trim(input('post.captcha')); //获取表单验证码
	    	if(!captcha_check($captcha)){	//通过验证码类自动验证函数
		       $this->error('验证码错误');   //失败则跳转失败页面
		    }else{//成功后开始验证账号密码
	    	$data['username'] = trim(input('post.username'));	
	    	//获取表单用户名,去掉多余空格
	    	$data['password'] = trim(input('post.password'));
	    	//获取表单密码吗,去掉多余空格
	    	$validate = validate('Check'); //实例化验证器Check
    		if(!$validate->check($data)){	//验证获取的信息失败则跳转失败信息
			    $this->error($validate->getError());
			}
			$online = intval(input('post.online'));	//是否保持一直在线
			$password = MD5(MD5($data['password']).md5('ChuckWe'));	
			//MD5加密前台用户输入的密码
			$rs = Db::table('cwcms_users')->where('username',$data['username'])->where('password',$password)->find();	
			//对比数据库，SQL这里根据用户名和加密的密码同时查询
				if(!$rs){
					$this->error('用户名密码错误');	//不存在SQL则返回失败
				}else{
					session('loginid',$rs['id']);	//存在则吧ID存入session
					session('slogin_time',time());	//登录时间存入session
					if($online){
						Session::set('login_expire',24*3600);	
						//如果勾选一直在线，则保持24小时在线，当然因为存入session关闭浏览器之后就会失效，保存在cookie则没有
					}else{
						Session::set('login_expire',600);	//十分钟
					}
					session('username',$rs['username']);	//用户名存入session	
					$update['id'] = $rs['id'];	
					$update['last_login_time'] = time();
					$update['last_login_ip'] = Request::instance()->ip();
					$up = Db::table('cwcms_users')->update($update);	
					//登录信息更新用户表
					$this->success('登录成功','admin/index/index',3);
				}
			}
		}
    	return $this->fetch();
    }
    public function logout()
    {
    	Session::clear();
    	$this->success('退出成功','admin/login/login',2);
    }
}