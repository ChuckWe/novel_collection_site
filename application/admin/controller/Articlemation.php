<?php
namespace app\admin\controller;
use think\Request;
use think\Session;
use think\Db;
use think\Controller;
/**
 * 资讯管理控制器
 * @authors ChuckWe (s312906342@gmail.com)
 * @date    2018-06-07 09:49:46
 * @version $Id$
 */

class Articlemation extends Common {
    
    public function article_list(){
    	return $this->fetch();
        
    }

    public function article_class(){
    	return $this->fetch();
    }
}