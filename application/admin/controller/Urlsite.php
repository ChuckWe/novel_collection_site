<?php
namespace app\admin\controller;
use think\Request;
use think\Log;
use think\Db;
use QL\QueryList;

/**
 * 源节点管理
 * @authors ChuckWe (s312906342@gmail.com)
 * @date    2018-05-28 20:05:42
 * @version $Id$
 */

class Urlsite extends Common {
    
    public function urlsite_list(){
    	$list = Db::name('urlsite')->select();
    	$this->assign('list',$list);
    	

        return $this->fetch();
    }

    public function urlsite_add(){
    	if(Request::instance()->isAjax()){
        	$data['urlsitename'] =trim(input('param.urlsitename'));
        	$data['searchurl'] = trim(input('param.searchurl'));
        	$data['searchlist'] = trim(input('param.searchlist'));
        	$data['searchmenu'] = trim(input('param.searchmenu'));
        	$data['searchar'] = trim(input('param.searchar'));
        	if(empty($data['urlsitename']) || empty($data['searchurl']) || empty($data['searchlist']) || empty($data['searchmenu']) || empty($data['searchar'])){
        		$json['code'] = 0;
        		$json['msg'] = '所有项都不能为空';
        		return json($json);
        	}

    	   $urlsite = Db::name('urlsite')->insertGetId($data);
        	if($urlsite){
        		$json['code'] = 200;
        		$json['msg'] = '添加成功';
        		return json($json);
        	}else{
        		$json['code'] = 0;
        		$json['msg'] = '添加失败,检查参数!';
        		return json($json);
        	}	
    	}
    	
    	return $this->fetch();
    }

    public function urlsite_edit(){
        $id = request()->param('id');
        if(Request::instance()->isAjax()){
            $data = request()->post();
            if(empty($data['urlsitename']) || empty($data['searchurl']) || empty($data['searchlist']) || empty($data['searchmenu']) || empty($data['searchar'])){
                return error('所有项都不能为空');
            }
            $urlsite = Db::name('urlsite')->update($data);
            if($urlsite){
                return json('修改成功!');
            }else{
                return error('修改成功,检查参数!');
            }   
        }
        $urlsite = Db::name('urlsite')->find($id);
        $this->assign('urlsite',$urlsite);
        return $this->fetch();
    }
}