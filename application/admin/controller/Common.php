<?php
namespace app\admin\controller;
use think\Controller;
use think\Validate;
use think\Request;
use think\Db;
use think\Session;
use think\Config;
use think\Jump;
use think\Auth;
use lib\Data;
/**
 * 公共控制器，权限登录以及session
 * @authors ChuckWe (s312906342@gmail.com)
 * @date    2018-04-19 14:37:12
 * @version $Id$
 */

class Common extends Controller {
    
   public $menu;
   protected function _initialize()
   {
   		
   		if(!Session::get('loginid')){
   			$this->error('未登录，请返回登录','admin/login/login');
   		}
   		if(time() - Session::get('slogin_time') > Session::get('login_expire') ){
   			Session::clear();
   			$this->error('登录信息已过期，请返回登录',$this->redirect('admin/login/login'));
   		}
         $this->auth(Session::get('loginid'));
         $userinfo = Db::name('users')->where('id',Session::get('loginid'))->find();
         $this->assign('id',Session::get('loginid'));
         $this->menu = Db::query("select * from cwcms_menu");
         $tree = new \lib\Data();
         $menu_list = $tree->channelLevel($this->menu);
         $this->assign('menu',$menu_list);

   }

   protected function auth($id)
   {
      $auth = new Auth();
      $request = Request::instance();
      $rule_name=$request->module().'/'.$request->controller().'/'.$request->action();
      $result=$auth->check($rule_name,$id);
      if(!$result){
        $this->error('您没有权限访问','admin/index/welcome',2);
      }
   }

   
}