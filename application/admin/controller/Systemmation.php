<?php
namespace app\admin\controller;
use think\Request;
use think\Session;
use think\Db;
/**
 * 系统管理控制器
 * @authors ChuckWe (s312906342@gmail.com)
 * @date    2018-06-07 10:37:42
 * @version $Id$
 */

class Systemmation extends Common {
    
    public function system_category(){
        return $this->fetch();
    }

    public function system_category_add(){
    	return $this->fetch();
    }
}