<?php
namespace app\admin\controller;
use think\Request;
use think\Session;
use think\Controller;
use think\Db;
use think\Log;
/**
 * @会员管理
 * @authors ChuckWe (s312906342@gmail.com)
 * @date    2018-07-17 08:26:41
 * @version $Id$
 */

class Member extends Common {
    
    public function member_list(){
    	$member_list = Db::name('members')->select();
    	$this->assign('member_list',$member_list);
    	return $this->fetch();
    }

    public function member_del(){
    	if(request()->isPost()){
    		$ids = request()->post();
    		$del = Db::name('members')->delete($ids);
    		if($del){
    			return success('删除成功!');
    		}else{
    			return error('删除失败!');
    		}
    	}
    }

    public function member_ctrl(){
    	$ctrl = trim(input('param.fun'));
    	$this->assign('ctrl',$ctrl);
    	
    	if($ctrl == 'add'){

		    	if(Request::instance()->isAjax()){
		    		$data = array(
		    			'username' => trim(input('param.username')),
		    			'nickname' => trim(input('param.nickname')),
		    			'phone' => trim(input('param.mobile')),
		    			'email' => trim(input('param.email')),
		    			'remark' => trim(input('param.beizhu')),
		    			'createtime' => time(),
		    		);
		    		$check_name = Db::name('members')->where('username',$data['username'])->find();
		    		if($check_name){
		    			return ajaxShow('','400','账号名已存在,请重新创建!','2');
		    		}
		    		$opition = array('cost'=>8);
		    		$password = trim(input('param.password'));
		    		$data['password'] = password_hash($password,PASSWORD_BCRYPT,$opition);
		    		$result = Db::name('members')->insert($data);
		    		if($result){
		    			return ajaxShow('','200','添加成功！','1');
		    		}else{
		    			return ajaxShow('','404','添加失败！请检查参数！','2');
		    		}
		    	}

    		return $this->fetch();

	    	}
	    	if($ctrl == 'edit'){
	    		return $this->fetch();
	    	}
    }
}