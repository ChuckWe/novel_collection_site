<?php
namespace app\admin\validate;
use think\Validate;
/**
 * 登录验证器，验证账号密码以及验证码
 * @authors ChuckWe (s312906342@gmail.com)
 * @date    2018-04-19 16:33:08
 * @version $Id$
 */

class Check extends Validate {
    
    protected $rule = [
        'name' => 'token',
    	'username' => 'require|max:8|checkName:',
    	'password' => 'require|checkKeywords:',
    ];

    protected $msg = [
    	'name.require' => '用户名必填',
    	'name.max'	=> '名称最多不超过8位',
    	'password.require'	=> '密码必填',
    ];

    protected function checkName($value)
    {
    	 $rs = preg_match('/^[A-Za-z0-9]{4,40}$/',$value);
    	 if(!$rs){
    	 	return '用户名不能为汉字';
    	 }else{
    	 	return true;
    	 }
    }

    protected function checkKeywords($value)
    {
    	$rs = preg_match('/^[a-zA-Z]\w{5,17}$/',$value);
    	if(!$rs){
    	 	return '密码字母开头，6-18之间';
    	 }else{
    	 	return true;
    	 }

    }
    
}

