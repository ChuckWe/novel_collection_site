<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:31:"../template/web/admin\role.html";i:1543565538;s:53:"G:\www\mayun\readercms\template\web\common\_meta.html";i:1543565538;s:55:"G:\www\mayun\readercms\template\web\common\_footer.html";i:1543565538;}*/ ?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="Bookmark" href="/favicon.ico" >
<link rel="Shortcut Icon" href="/favicon.ico" />
<!--[if lt IE 9]>
<script type="text/javascript" src="/web/lib/html5shiv.js"></script>
<script type="text/javascript" src="/web/lib/respond.min.js"></script>
<![endif]-->
<link rel="stylesheet" type="text/css" href="/web/static/h-ui/css/H-ui.min.css" />
<link rel="stylesheet" type="text/css" href="/web/static/h-ui.admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="/web/lib/Hui-iconfont/1.0.8/iconfont.css" />
<link rel="stylesheet" type="text/css" href="/web/static/h-ui.admin/skin/default/skin.css" id="skin" />
<link rel="stylesheet" type="text/css" href="/web/static/h-ui.admin/css/style.css" />
<!--[if IE 6]>
<script type="text/javascript" src="/web/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->
<title>角色管理</title>
</head>
<body>
	
<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 管理员管理 <span class="c-gray en">&gt;</span> 角色管理 <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
<div class="page-container">
	<div class="cl pd-5 bg-1 bk-gray"> <span class="l"> <a href="javascript:;" onclick="role_del(this,'0')" class="btn btn-danger radius"><i class="Hui-iconfont">&#xe6e2;</i> 批量删除</a> <a class="btn btn-primary radius" href="javascript:;" onclick="admin_role_add('添加角色','<?php echo url('admin/admin/role_add'); ?>','800')"><i class="Hui-iconfont">&#xe600;</i> 添加角色</a> </span> <span class="r">共有数据：<strong><?php echo $counts; ?></strong> 条</span> </div>
	<table class="table table-border table-bordered table-hover table-bg">
		<thead>
			<tr>
				<th scope="col" colspan="6">角色管理</th>
			</tr>
			<tr class="text-c">
				<th width="25"><input type="checkbox" value=""  onclick="checkall();"  id="all"></th>
				<th width="40">ID</th>
				<th width="200">角色名</th>
				<th>用户列表</th>
				<th width="300">描述</th>
				<th width="70">操作</th>
			</tr>
		</thead>
		<tbody>
			<?php if(is_array($roles) || $roles instanceof \think\Collection || $roles instanceof \think\Paginator): $i = 0; $__LIST__ = $roles;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$r): $mod = ($i % 2 );++$i;?>
			<tr class="text-c">
				<td><?php if($r['id'] != 4): ?><input type="checkbox" value="<?php echo $r['id']; ?>" name="del" id="del"><?php endif; ?></td>
				<td><?php echo $r['id']; ?></td>
				<td><?php echo $r['title']; ?></td>
				<td>
				<?php if(empty($r['uids']) || (($r['uids'] instanceof \think\Collection || $r['uids'] instanceof \think\Paginator ) && $r['uids']->isEmpty())): ?>
				<a href="#">分组暂无管理人员</a>
				<?php else: if(is_array($r['uids']) || $r['uids'] instanceof \think\Collection || $r['uids'] instanceof \think\Paginator): $i = 0; $__LIST__ = $r['uids'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$u): $mod = ($i % 2 );++$i;?>
				<a href="#">
				<?php echo $u['username']; ?>
				</a>,
				<?php endforeach; endif; else: echo "" ;endif; endif; ?>
				</td>
				<td><?php echo $r['desc']; ?></td>
				<td class="f-14">
					<?php if($r['id'] != '4'): ?>
					<a title="编辑" href="javascript:;" onclick="admin_role_edit('角色编辑','<?php echo url('admin/admin/role_edit'); ?>','<?php echo $r['id']; ?>')" style="text-decoration:none"><i class="Hui-iconfont">&#xe6df;</i></a> <a title="删除" href="javascript:;" onclick="role_del(this,'<?php echo $r['id']; ?>')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6e2;</i></a></td>
					<?php endif; ?>
			</tr>
			<?php endforeach; endif; else: echo "" ;endif; ?>
			
		</tbody>
	</table>
</div>
<script type="text/javascript" src="/web/lib/jquery/1.9.1/jquery.min.js"></script> 
<script type="text/javascript" src="/web/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/web/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/web/static/h-ui.admin/js/H-ui.admin.js"></script> 
<script type="text/javascript" src="/web/lib/laypage/1.2/laypage.js"></script>

<!--请在下方写此页面业务相关的脚本-->

<script type="text/javascript">
/*管理员-角色-添加*/
function admin_role_add(title,url,w,h){
	layer_show(title,url,w,h);
}
/*管理员-角色-编辑*/
function admin_role_edit(title,url,id,w,h){
	if (title == null || title == '') {
		title=false;
	};
	if (w == null || w == '') {
		w=800;
	};
	if (h == null || h == '') {
		h=($(window).height() - 50);
	};
	layer.open({
		type: 2,
		area: [w+'px', h +'px'],
		fix: false, 
		maxmin: true,
		shade:0.4,
		title: title,
		content: '<?php echo url('admin/admin/role_edit'); ?>?id='+id,
	});
}
/*管理员-角色-删除*/
function role_del(obj,id){
	var obj = document.getElementsByName("del");//选择所有name="interest"的对象，返回数组  
    var del=[];//如果这样定义var s;变量s中会默认被赋个null值
    for(var i=0;i<obj.length;i++){
        if(obj[i].checked) //取到对象数组后，我们来循环检测它是不是被选中
        del[i]=obj[i].value;  //如果选中，将value添加到变量s中  
    }
	var num = $.inArray('4',del);
	if(num != '-1') {
		layer.alert('管理员分组不能删除!');
		return false;
	}  
	if (del == null || del == '') {
		layer.alert('删除选择不能为空!');
		return false;
	}
	layer.confirm('角色删除须谨慎，确认要删除吗？',function(index){
		$.ajax({
			type: 'GET',
			url: '<?php echo url('admin/admin/role_del'); ?>?id='+id,
			data:{del:del},
			dataType: 'json',
			success: function(data){
				if (!data.code){
				layer.msg(data.msg,{icon:2,time:2000});	
				} else {
				layer.msg(data.msg,{icon:1,time:1000});
				}
				setTimeout(function () {
                    window.location.reload();
                }, 1000);
				
			},
			error:function(data) {
				console.log(data.msg);
			},
		});		
	});
}

function checkall() {  
var all=document.getElementById('all');//获取到点击全选的那个复选框的id  
var one=document.getElementsByName('del');//获取到复选框的名称  
//因为获得的是数组，所以要循环 为每一个checked赋值  
for(var i=0;i<one.length;i++){  
one[i].checked=all.checked; //直接赋值不就行了嘛  
}  
}


</script>
</body>
</html>