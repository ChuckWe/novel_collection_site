<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:37:"../template/web/admin\admin_list.html";i:1543565538;s:53:"G:\www\mayun\readercms\template\web\common\_meta.html";i:1543565538;s:55:"G:\www\mayun\readercms\template\web\common\_footer.html";i:1543565538;}*/ ?>
﻿<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="Bookmark" href="/favicon.ico" >
<link rel="Shortcut Icon" href="/favicon.ico" />
<!--[if lt IE 9]>
<script type="text/javascript" src="/web/lib/html5shiv.js"></script>
<script type="text/javascript" src="/web/lib/respond.min.js"></script>
<![endif]-->
<link rel="stylesheet" type="text/css" href="/web/static/h-ui/css/H-ui.min.css" />
<link rel="stylesheet" type="text/css" href="/web/static/h-ui.admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="/web/lib/Hui-iconfont/1.0.8/iconfont.css" />
<link rel="stylesheet" type="text/css" href="/web/static/h-ui.admin/skin/default/skin.css" id="skin" />
<link rel="stylesheet" type="text/css" href="/web/static/h-ui.admin/css/style.css" />
<!--[if IE 6]>
<script type="text/javascript" src="/web/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->
<title>管理员列表</title>
</head>
<body>

<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span> 管理员管理 <span class="c-gray en">&gt;</span> 管理员列表 <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a></nav>
<div class="page-container">
	<form action="" name="form1" method="post">
	<div class="text-c"><!-- 日期范围：
		<input type="data" onfocus="" id="datemin" class="input-text Wdate" style="width:120px;">
		-
		<input type="text" onfocus="" id="datemax" class="input-text Wdate" style="width:120px;"-->
		<input type="text" class="input-text" name="search" style="width:250px" placeholder="输入管理员名称" id="" >
		<button type="submit" class="btn btn-success" id="" ><i class="Hui-iconfont">&#xe665;</i> 搜用户</button>
	</div>
	</form>
	<div class="cl pd-5 bg-1 bk-gray mt-20"> <span class="l"><a href="javascript:;" onclick="admin_del(this,'0')" class="btn btn-danger radius"><i class="Hui-iconfont">&#xe6e2;</i> 批量删除</a> <a href="javascript:;" onclick="admin_add('添加管理员','<?php echo url('admin/admin/add'); ?>','800','500')" class="btn btn-primary radius"><i class="Hui-iconfont">&#xe600;</i> 添加管理员</a></span> <span class="r">共有数据：<strong><?php echo $counts; ?></strong> 条</span> </div>
	<table class="table table-border table-bordered table-bg">
		<thead>
			<tr>
				<th scope="col" colspan="9">员工列表</th>
			</tr>
			<tr class="text-c">
				<th width="25"><input type="checkbox" name="" value="" id="all" onclick="checkall();"></th>
				<th width="40">ID</th>
				<th width="150">登录名</th>
				<th width="90">手机</th>
				<th width="150">邮箱</th>
				<th>角色</th>
				<th width="130">加入时间</th>
				<th width="100">是否已启用</th>
				<th width="100">操作</th>
			</tr>
		</thead>
		<tbody>
			<?php if(is_array($users) || $users instanceof \think\Collection || $users instanceof \think\Paginator): $i = 0; $__LIST__ = $users;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$list): $mod = ($i % 2 );++$i;?>
			<tr class="text-c">
				
				<td><?php if($list['id'] != 1): ?><input type="checkbox" value="<?php echo $list['id']; ?>" name="del"><?php endif; ?></td>
				<td><?php echo $list['id']; ?></td>
				<td><?php echo $list['username']; ?></td>
				<td><?php echo $list['phone']; ?></td>
				<td><?php echo $list['email']; ?></td>
				<td><?php echo $list['group']['title']; ?></td>
				<td><?php echo date('Y-m-d H:m:s',$list['register_time']); ?></td>
				<?php if($list['del'] == 1): ?> 
				<td class="td-status"><span class="label label-success  radius " >已启用</span></td>
				<?php else: ?>
				<td class="td-status"><span class="label radius">已停用</span></td>
				<?php endif; ?>
				<td class="td-manage">
				<?php if($list['id'] != 1): if($list['del'] == 1): ?>
					<a style="text-decoration:none"  onClick="admin_status('<?php echo $list['id']; ?>','0')" href="javascript:;"  title="停用"><i class="Hui-iconfont">&#xe631;</i></a>
				<?php else: ?>
					<a style="text-decoration:none" onClick="admin_status('<?php echo $list['id']; ?>','1')" href="javascript:;" title="启用"><i class="Hui-iconfont">&#xe615;</i></a>
				<?php endif; endif; ?>
					<a title="编辑" href="javascript:;" onclick="admin_edit('编辑','<?php echo $list['id']; ?>','800','500')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6df;</i></a>
				<?php if($list['id'] != 1): ?>
					<a title="删除" href="javascript:;" onclick="admin_del(this,'<?php echo $list['id']; ?>')" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6e2;</i></a></td>
				<?php endif; ?>
			</tr>
			<?php endforeach; endif; else: echo "" ;endif; ?>
		</tbody>
	</table>
</div>
<script type="text/javascript" src="/web/lib/jquery/1.9.1/jquery.min.js"></script> 
<script type="text/javascript" src="/web/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/web/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/web/static/h-ui.admin/js/H-ui.admin.js"></script> 
<script type="text/javascript" src="/web/lib/laypage/1.2/laypage.js"></script>

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript">
/*
	参数解释：
	title	标题
	url		请求的url
	id		需要操作的数据id
	w		弹出层宽度（缺省调默认值）
	h		弹出层高度（缺省调默认值）
*/
/*管理员-增加*/
function admin_add(title,url,w,h){
	if (title == null || title == '') {
		title=false;
	};
	if (url == null || url == '') {
		url="404.html";
	};
	if (w == null || w == '') {
		w=800;
	};
	if (h == null || h == '') {
		h=($(window).height() - 50);
	};
	layer.open({
		type: 2,
		area: [w+'px', h +'px'],
		fix: false, 
		maxmin: true,
		shade:0.4,
		title: title,
		content: url,
	});
}
/*管理员-删除*/
function admin_del(obj,id){
	var obj = document.getElementsByName("del");//选择所有name="interest"的对象，返回数组  
    var del=[];//如果这样定义var s;变量s中会默认被赋个null值
    for(var i=0;i<obj.length;i++){
        if(obj[i].checked) //取到对象数组后，我们来循环检测它是不是被选中
        del[i]=obj[i].value;  //如果选中，将value添加到变量s中  
    }
	layer.confirm('确认要删除吗？',function(index){
		$.ajax({
			type: 'get',
			url: '<?php echo url('admin/admin/del'); ?>?id='+id,
			data: {del:del},
			dataType: 'json',
			success: function(data){
				layer.msg(data.msg,{icon:data.icon,time:1000});
				setTimeout(function () {
                    window.location.reload();
                }, 1000)
			},
			error:function(data) {
				console.log(data.msg);
			},
		});		
	});
}

/*管理员-编辑*/
function admin_edit(title,id,w,h){
	if (title == null || title == '') {
		title=false;
	};
	if (w == null || w == '') {
		w=800;
	};
	if (h == null || h == '') {
		h=($(window).height() - 50);
	};
	layer.open({
		type: 2,
		area: [w+'px', h +'px'],
		fix: false, 
		maxmin: true,
		shade:0.4,
		title: title,
		content: '<?php echo url('admin/admin/edit'); ?>?id='+id,
	});
}
/*管理员-停用*/
function admin_status(id,del){
	layer.confirm('确认要更改状态吗？',function(index){
		//此处请求后台程序，下方是成功后的前台处理……
		$.ajax({
			type: 'POST',
			url: '<?php echo url('admin/admin/admin_list'); ?>',
			dataType: 'json',
			data:{"id":id,"del":del},
			success: function(data){
				layer.msg(data.msg,{icon:1,time:1000}, function(){
					window.location.reload();
				});
			},
			error:function(data) {
				console.log(data.msg);
			},
		});		
		
	});
}

/*管理员-全选*/
function checkall() {  
	var all=document.getElementById('all');//获取到点击全选的那个复选框的id  
	var one=document.getElementsByName('del');//获取到复选框的名称  
	//因为获得的是数组，所以要循环 为每一个checked赋值  
	for(var i=0;i<one.length;i++){  
	one[i].checked=all.checked; //直接赋值不就行了嘛  
	}  
}

</script>
</body>
</html>