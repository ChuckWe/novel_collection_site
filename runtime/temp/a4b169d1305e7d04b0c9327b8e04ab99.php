<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:36:"../template/web/admin\role_edit.html";i:1543565538;s:53:"G:\www\mayun\readercms\template\web\common\_meta.html";i:1543565538;s:55:"G:\www\mayun\readercms\template\web\common\_footer.html";i:1543565538;}*/ ?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="Bookmark" href="/favicon.ico" >
<link rel="Shortcut Icon" href="/favicon.ico" />
<!--[if lt IE 9]>
<script type="text/javascript" src="/web/lib/html5shiv.js"></script>
<script type="text/javascript" src="/web/lib/respond.min.js"></script>
<![endif]-->
<link rel="stylesheet" type="text/css" href="/web/static/h-ui/css/H-ui.min.css" />
<link rel="stylesheet" type="text/css" href="/web/static/h-ui.admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="/web/lib/Hui-iconfont/1.0.8/iconfont.css" />
<link rel="stylesheet" type="text/css" href="/web/static/h-ui.admin/skin/default/skin.css" id="skin" />
<link rel="stylesheet" type="text/css" href="/web/static/h-ui.admin/css/style.css" />
<!--[if IE 6]>
<script type="text/javascript" src="/web/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->

<title>编辑网站角色 - <?php echo $groupinfo['title']; ?> </title>
<meta name="keywords" content="H-ui.admin v3.1,H-ui网站后台模版,后台模版下载,后台管理系统模版,HTML后台模版下载">
<meta name="description" content="H-ui.admin v3.1，是一款由国人开发的轻量级扁平化网站后台模板，完全免费开源的网站后台管理系统模版，适合中小型CMS后台系统。">
</head>
<body>
<article class="page-container">
	<form action="" method="post" class="form form-horizontal" id="form-role-edit">
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>角色名称：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text" value="<?php echo $groupinfo['title']; ?>" placeholder=""  name="title">
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>备注：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text" value="<?php echo $groupinfo['desc']; ?>" placeholder="" id="" name="desc">
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>网站角色：</label>
			<div class="formControls col-xs-8 col-sm-9">
				
					<?php if(is_array($rules) || $rules instanceof \think\Collection || $rules instanceof \think\Paginator): $i = 0; $__LIST__ = $rules;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vol): $mod = ($i % 2 );++$i;?>
						<dl class="permission-list">
						<dt>
						<label>
							<input type="checkbox" value="<?php echo $vol['id']; ?>" name="ftitle[]" id="ftitle" <?php if(in_array(($vol['id']), is_array($functions)?$functions:explode(',',$functions))): ?>checked<?php endif; ?>>
							<?php echo $vol['title']; ?></label>
						</dt>
					<?php if(isset($vol['pid'])): if(is_array($vol['pid']) || $vol['pid'] instanceof \think\Collection || $vol['pid'] instanceof \think\Paginator): $i = 0; $__LIST__ = $vol['pid'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($i % 2 );++$i;?>
						<dd>
						
							 <dl class="cl permission-list2">
								<dt>
									<label class="">
										<input type="checkbox" value="<?php echo $item['id']; ?>" name="ftitle[]" <?php if(in_array(($item['id']), is_array($functions)?$functions:explode(',',$functions))): ?>checked<?php endif; ?> >
										<?php echo $item['title']; ?></label>
								</dt>
					<?php if(isset($item['pid'])): if(is_array($item['pid']) || $item['pid'] instanceof \think\Collection || $item['pid'] instanceof \think\Paginator): $i = 0; $__LIST__ = $item['pid'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$k): $mod = ($i % 2 );++$i;?>
								<dd>
									<label class="">
									<input type="checkbox" value="<?php echo $k['id']; ?>" name="ftitle[]" <?php if(in_array(($k['id']), is_array($functions)?$functions:explode(',',$functions))): ?>checked<?php endif; ?> >
									<?php echo $k['title']; ?></label>
								</dd>
					<?php endforeach; endif; else: echo "" ;endif; endif; ?>
							</dl>
					
							
						
						</dd>
					<?php endforeach; endif; else: echo "" ;endif; endif; ?>
						</dl>
					<?php endforeach; endif; else: echo "" ;endif; ?>
					
				
			</div>
			<input type="text" name="id" value="<?php echo $groupinfo['id']; ?>" style="display: none;" readonly="" />
		</div>
		<div class="row cl">
			<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
				<a class="btn btn-success radius" onclick="ajaxsubmit();"> 确定</a>
			</div>
		</div>
	</form>
</article>

<script type="text/javascript" src="/web/lib/jquery/1.9.1/jquery.min.js"></script> 
<script type="text/javascript" src="/web/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/web/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/web/static/h-ui.admin/js/H-ui.admin.js"></script> 
<script type="text/javascript" src="/web/lib/laypage/1.2/laypage.js"></script>

<!--请在下方写此页面业务相关的脚本-->

<script type="text/javascript">
$(function(){
	$(".permission-list dt input:checkbox").click(function(){
		$(this).closest("dl").find("dd input:checkbox").prop("checked",$(this).prop("checked"));
	});
	$(".permission-list2 dd input:checkbox").click(function(){
		var l =$(this).parent().parent().find("input:checked").length;
		var l2=$(this).parents(".permission-list").find(".permission-list2 dd").find("input:checked").length;
		if($(this).prop("checked")){
			$(this).closest("dl").find("dt input:checkbox").prop("checked",true);
			$(this).parents(".permission-list").find("dt").first().find("input:checkbox").prop("checked",true);
		}
		else{
			if(l==0){
				$(this).closest("dl").find("dt input:checkbox").prop("checked",false);
			}
			if(l2==0){
				$(this).parents(".permission-list").find("dt").first().find("input:checkbox").prop("checked",false);
			}
		}
	});
});

function ajaxsubmit(){
		$.ajax({
			type:'post',
			url:'<?php echo url('admin/admin/role_edit'); ?>',
			dataType:'json',
			data: $('#form-role-edit').serialize(),
			success: function(data){
				if(data.code != 200){
					layer.alert(data.msg, {icon: data.icon});
				}else{
					layer.msg(data.msg,{icon:data.icon,time:1000},function(){
					window.parent.location.reload();
					var index = parent.layer.getFrameIndex(window.name);
					parent.layer.close(index);
					});
				}
			},
            error: function(XmlHttpRequest, textStatus, errorThrown){
            	console.log(XmlHttpRequest, textStatus, errorThrown);
				layer.alert('添加失败!', {icon: 2});
			}
		});
	}
</script>
<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>