<?php if (!defined('THINK_PATH')) exit(); /*a:3:{s:41:"../template/web/urlsite\urlsite_edit.html";i:1543568143;s:53:"G:\www\mayun\readercms\template\web\common\_meta.html";i:1543565538;s:55:"G:\www\mayun\readercms\template\web\common\_footer.html";i:1543565538;}*/ ?>
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="Bookmark" href="/favicon.ico" >
<link rel="Shortcut Icon" href="/favicon.ico" />
<!--[if lt IE 9]>
<script type="text/javascript" src="/web/lib/html5shiv.js"></script>
<script type="text/javascript" src="/web/lib/respond.min.js"></script>
<![endif]-->
<link rel="stylesheet" type="text/css" href="/web/static/h-ui/css/H-ui.min.css" />
<link rel="stylesheet" type="text/css" href="/web/static/h-ui.admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="/web/lib/Hui-iconfont/1.0.8/iconfont.css" />
<link rel="stylesheet" type="text/css" href="/web/static/h-ui.admin/skin/default/skin.css" id="skin" />
<link rel="stylesheet" type="text/css" href="/web/static/h-ui.admin/css/style.css" />
<!--[if IE 6]>
<script type="text/javascript" src="/web/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->

<title>新建节点 - 源节点管理</title>
<meta name="keywords" content="H-ui.admin v3.1,H-ui网站后台模版,后台模版下载,后台管理系统模版,HTML后台模版下载">
<meta name="description" content="H-ui.admin v3.1，是一款由国人开发的轻量级扁平化网站后台模板，完全免费开源的网站后台管理系统模版，适合中小型CMS后台系统。">
</head>
<style>
	.input_text{
		height: 50px;
	}
</style>
<body>
<article class="page-container">
	<form  class="form form-horizontal" id="form-urlsite-add">
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3">节点名称：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text" value="<?php echo $urlsite['urlsitename']; ?>" placeholder="" name="urlsitename">
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3">节点搜索链接：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text" value="<?php echo $urlsite['searchurl']; ?>" placeholder="" id="" name="searchurl">
				形式为www.baidu.com/search
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3">列表页搜索规则：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text" value="<?php echo $urlsite['searchlist']; ?>" placeholder="" id="" name="searchlist">
				以|分割参数和属性,用,分割项目
				<p>第一项为书名bookname,第二项为书籍url,第三项为最新更新章节lastar,第四项为作者author,第五项为最新时间uptime</p>
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3">目录页规则：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text" value="<?php echo $urlsite['searchmenu']; ?>" placeholder="" id="" name="searchmenu">
				以|分割参数和属性,用,分割项目
				<p>第一项为书名bookname,第二项为书籍封面bookimg,第三项为作者author,第四项为字数bookwords,第五项为状态state,第六项为简介intro,第七项为章节名字menutitle,第八项为章节url</p>
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3">内容页规则：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text" value="<?php echo $urlsite['searchar']; ?>" placeholder="" id="" name="searchar">
				以|分割参数和属性,用,分割项目
				<p>第一项为章节名字menutitle,第二项为正文内容article,第三项为上一章upurl,第四项为下一章nexturl,第五项目录menuurl</p>
			</div>
		</div>  
		
		<div class="row cl">
			<input type="hidden" name="id" value="<?php echo $urlsite['id']; ?>">
			<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
				<a class="btn btn-success radius" onclick="ajaxsubmit();"> 确定</a>
			</div>
		</div>
	</form>
</article>

<script type="text/javascript" src="/web/lib/jquery/1.9.1/jquery.min.js"></script> 
<script type="text/javascript" src="/web/lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="/web/static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="/web/static/h-ui.admin/js/H-ui.admin.js"></script> 
<script type="text/javascript" src="/web/lib/laypage/1.2/laypage.js"></script>


<script type="text/javascript">
function ajaxsubmit(){
		$.ajax({
			type:'post',
			url:'<?php echo url('admin/urlsite/urlsite_edit'); ?>',
			dataType:'json',
			data: $('#form-urlsite-add').serialize(),
			success: function(data){
				if(data.code != 200){
					layer.alert(data.msg, {icon: data.icon});
				}else{
					layer.msg(data.msg,{icon:data.icon,time:1000},function(){
					window.parent.location.reload();
					var index = parent.layer.getFrameIndex(window.name);
					parent.layer.close(index);
					});
				}
			},
            error: function(XmlHttpRequest, textStatus, errorThrown){
            	console.log(XmlHttpRequest, textStatus, errorThrown);
				layer.alert('规则标识已存在!', {icon: 2});
			}
		});
	}
</script>
<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>