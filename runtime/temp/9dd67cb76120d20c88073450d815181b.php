<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:41:"../template/home/piclist\search_list.html";i:1530752472;}*/ ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>图表</title>
	<link rel="stylesheet" type="text/css" href="/home/layui/css/layui.css" />
	<script type="text/javascript" src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
	<script type="text/javascript" src="/home/layui/layui.js"></script>
	<script type="text/javascript" src="/home/js/echarts.common.min.js"></script>
	<script type="text/javascript" src="/home/js/tanchuang.common.js"></script>

</head>
<style type="text/css">

</style>
<body>
	<div class="layui-container">
		<div class="layui-row" style="text-align: center;margin-top: 20px;" >
			<div class="layui-col-lg12 layui-col-space10">
				<div class="layui-inline">
					 <input type="text" placeholder="搜索起始时间" class="layui-input" id="test1" name="starttime">
				</div>
				<div class="layui-inline">
					<input type="text" placeholder="搜索截止时间" class="layui-input" id="test2" name="endtime">
				</div>
				<button class="layui-btn layui-btn-radius" onclick="jsonlist()">搜索查询</button>
			</div>
		</div>
		<div class="layui-row" >
			<div class="layui-col-lg12" style="text-align: center;padding-top:50px;">
				<div id="main" style="margin:0 auto;width: 640px;height: 320px;"></div>
			</div>
		</div>
	</div>
 <!-- 为ECharts准备一个具备大小（宽高）的Dom -->
    
    <script type="text/javascript">
    	layui.use('laydate', function(){
		  var laydate = layui.laydate;
		  
		  //执行一个laydate实例
		  laydate.render({
		    elem: '#test1' //指定元素
		    ,type:'datetime'
		  });
		  laydate.render({
		  	elem: '#test2'
		  	,type:'datetime'
		  });
		});
    </script>
    <script type="text/javascript">
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('main'));
        echo = <?php echo $json; ?>;
        // 指定图表的配置项和数据
        var option = {
            title: {
                text: '图例校园情况表'
            },
            tooltip: {},
            legend: {
                data:['次数']
            },
            xAxis: {
                data: ['班级圈','班级通知','校园通知','发布作业','请假','留言','相册']
            },
            yAxis: {},
            series: [{
                name: ['次数'],
                type: 'bar',
                data: echo.numeric,
                 label: {
			      normal: {
			          show: true,
			          position: 'top',
			          textStyle: {
			            color: 'black'
			          }
			      }
			   }
            }],

        };
        myChart.setOption(option);
        function jsonlist(){
        	starttime = $('#test1').val();
        	endtime = $('#test2').val();
        	url = window.location.href;
        	url = url.replace('search_list','search_ajax');
        	$.ajax({
        		type:"post",
        		url:url,
        		dataType:'json',
        		data:{starttime:$('#test1').val(),endtime:$('#test2').val()},
        		success:function(data){
        			shot(data.msg,data.status);
        			option.series = [{data:data.numeric}];
        			 // 使用刚指定的配置项和数据显示图表。
        			myChart.setOption(option);
        		},
        		error:function(xhr){
					console.log(xhr.responseText);
        			shot(xhr.responseText,0); 
        		}
        	});
        }
       
    </script>
</body>
</html>