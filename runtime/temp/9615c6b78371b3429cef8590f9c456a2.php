<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:38:"../template/home/home\search_list.html";i:1543569452;s:59:"G:\www\mayun\readercms\template\home\common\web_header.html";i:1543565538;}*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="/home/css/kanshu.css">
    <link rel="stylesheet" type="text/css" href="/home/css/one.css">
    <link rel="stylesheet" type="text/css" href="/home/css/two.css">
    <link rel="stylesheet" type="text/css" href="/home/css/xiangqing.css">
     <script type="text/javascript" src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
    <script type="text/javascript" src="/home/layer/layer.js"></script>
    <title>搜索列表页</title>
</head>
<body>
  
    <div>
        <div class="head">
        <div class="head_top">
            <div class="limit">
              <div class="head_left">
                  <ul>
                      <li><a href="<?php echo url('home/home/search'); ?>">电脑首页</a></li>
                      <li><a href="#">手机首页</a></li>
                  </ul>
              </div>  
              <div class="head_right">
                  <ul>
                      <li><a href="javascript:;" onclick="changeurl();">换源</a></li>
                  </ul>
              </div>
            </div>
        </div>
        <div class="limit  head_one">
            <div class="food">
               <div class="logo">
                   <img src="/home/Iconfont/logo.png" alt="">
               </div>
               <div class="search">
                   <form action="<?php echo url('home/home/search_list'); ?>" method="post">
                       <input type="text" class="logo_search" name="bookname" placeholder="作品名/作者">
                       <input type="submit" value="" class="button_search">
                   </form>  
               </div>
            </div>  
        </div>
      </div>
            
            <div class="limit list">
                
                <?php if(is_array($listinfo) || $listinfo instanceof \think\Collection || $listinfo instanceof \think\Paginator): $i = 0; $__LIST__ = $listinfo;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$k): $mod = ($i % 2 );++$i;?>
                <div class="bookinfo">
                    <div class="thumb"><a href="<?php echo url('home/book/xiangqing'); ?>?id=<?php echo $k['_id']; ?>&title=<?php echo $k['title']; ?>"><img src="<?php echo $k['cover']; ?>"></a>
                    </div>
                    <div class="info">
                    <h3 class="title"><a href="<?php echo url('home/book/xiangqing'); ?>?id=<?php echo $k['_id']; ?>"> <?php echo $k['title']; ?>__<?php echo $k['site']; ?></a></h3>
                    <p class="desc"><?php echo $k['shortIntro']; ?></p>
                    </div>
                    <div class="item_info">
                        <p><span>作者：</span><span><?php echo $k['author']; ?></span></p>
                        <p><span>状态：</span><span>连载中</span></p>
                        <p><span>更新时间：</span><span></span></p>
                        <p><span>最新章节：</span><a href="<?php echo $k['lastChapter']; ?>"><span><?php echo $k['lastChapter']; ?></span></a></p>
                    </div>
                </div>
                <?php endforeach; endif; else: echo "" ;endif; ?>
              
                
            </div>
<!--列表区域-->
            
            
            <div class="limit foot">
                <em>
                    本站所有小说为转载作品，所有章节均由网友上传，转载至本站只是为了宣传本书让更多读者欣赏。
                </em>
            </div>
        </div>
    </div>
</body>
</html>

<script type="text/javascript">
    function changeurl(){
    layer.open({
      type: 2,
      title: '选择节点',
      shadeClose: true,
      shade: false,
      maxmin: true, //开启最大化最小化按钮
      area: ['893px', '600px'],
      content: '<?php echo url('home/book/changeurl'); ?>'
    });
    }
</script>