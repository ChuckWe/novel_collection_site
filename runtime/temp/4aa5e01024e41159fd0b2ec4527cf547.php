<?php if (!defined('THINK_PATH')) exit(); /*a:2:{s:36:"../template/home/book\xiangqing.html";i:1543569354;s:59:"G:\www\mayun\readercms\template\home\common\web_header.html";i:1543565538;}*/ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="/home/css/kanshu.css">
    <link rel="stylesheet" type="text/css" href="/home/css/one.css">
    <link rel="stylesheet" type="text/css" href="/home/css/two.css">
    <link rel="stylesheet" type="text/css" href="/home/css/xiangqing.css">
     <script type="text/javascript" src="http://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"></script>
    <script type="text/javascript" src="/home/layer/layer.js"></script>
    <title>详情页</title>
</head>
<body>
    <div class="head">
        <div class="head_top">
            <div class="limit">
              <div class="head_left">
                  <ul>
                      <li><a href="<?php echo url('home/home/search'); ?>">电脑首页</a></li>
                      <li><a href="#">手机首页</a></li>
                  </ul>
              </div>  
              <div class="head_right">
                  <ul>
                      <li><a href="javascript:;" onclick="changeurl();">换源</a></li>
                  </ul>
              </div>
            </div>
        </div>
        <div class="limit  head_one">
            <div class="food">
               <div class="logo">
                   <img src="/home/Iconfont/logo.png" alt="">
               </div>
               <div class="search">
                   <form action="<?php echo url('home/home/search_list'); ?>" method="post">
                       <input type="text" class="logo_search" name="bookname" placeholder="作品名/作者">
                       <input type="submit" value="" class="button_search">
                   </form>  
               </div>
            </div>  
        </div>
      </div>
        <div>
            <div class="xiangqing_limit limit">
                <div class="jieshao left">
                    <img class="cover" style="width: 180px;height: 240px;" src="<?php echo $bookinfo['cover']; ?>" alt="">
                </div>
                    <div class="xiangqing left">
                        <h2><?php echo $bookinfo['title']; ?></h2>

                        <a href=""><?php echo $bookinfo['author']; ?></a>
                        <p>著</p>
                        <p><?php echo $bookinfo['longIntro']; ?></p>
                        <em><?php echo $bookinfo['wordCount']; ?></em>
                        <span>万字</span>
                        <em>|</em>
                        <span><?php echo $listinfo['name']; ?></span>
                        <span><?php echo $bookinfo['updated']; ?></span>
                    </div>
              
            </div>
        </div>
        <div>
            <div class="mulu_top limit">
                <a style="border-bottom: 3px solid red; color:red;" href="#">全部章节<span><?php echo count($listinfo['chapters']); ?>章</span></a>
            </div>
        </div>
        <div>
            <div class="mulu_down limit">
                <h2>正文卷</h2>
                <ul>
                  <?php if(is_array($listinfo['chapters']) || $listinfo['chapters'] instanceof \think\Collection || $listinfo['chapters'] instanceof \think\Paginator): $k = 0; $__LIST__ = $listinfo['chapters'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$item): $mod = ($k % 2 );++$k;?>
                   
                    <li><a href="<?php echo url('home/book/kanshu'); ?>?sid=<?php echo $sid; ?>&k=<?php echo $k; ?>&url=<?php echo $item['link']; ?>" title="<?php echo $item['title']; ?>"><?php echo mb_substr($item['title'],0,15,'utf-8'); ?></a></li>
                    
                   
                  <?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
            </div>
        </div>
        <div class="limit foot">
            <em>
                本站所有小说为转载作品，所有章节均由网友上传，转载至本站只是为了宣传本书让更多读者欣赏。
            </em>
        </div>

</body>
</html>
<script type="text/javascript">
    function changeurl(){
    layer.open({
      type: 2,
      title: '选择节点',
      shadeClose: true,
      shade: false,
      maxmin: true, //开启最大化最小化按钮
      area: ['893px', '600px'],
      content: '<?php echo url('home/book/changeurl'); ?>?id=<?php echo $bookinfo['_id']; ?>&title=<?php echo $bookinfo['title']; ?>'
    });
    }
</script>